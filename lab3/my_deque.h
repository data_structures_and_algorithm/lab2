#pragma once
#include <iostream>

using namespace std;

struct deque_node {
	int data;
	deque_node *next;
	deque_node *prev;
};
struct my_deque {
	deque_node *first;
	deque_node *last;
};
void create(my_deque *&deque, int data);
void push_back(my_deque *&deque, int data);
void push_front(my_deque *&deque, int data);
void pop_back(my_deque *&deque);
void pop_front(my_deque *&deque);
void write_rl(my_deque *deque);
void write_lr(my_deque *deque);