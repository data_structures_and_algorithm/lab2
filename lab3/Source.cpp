#include <Windows.h>
#include <iostream>
#include "my_stack.h"
#include "my_queue.h"
#include "my_deque.h"

using namespace std;

void writeMenu()
{
	cout << "1  - push to stack" << endl;
	cout << "2  - pop from stack" << endl;
	cout << "3  - top element" << endl;
	cout << "4  - reverse stack" << endl;
	cout << "5  - write stack" << endl;

	cout << "6  - create queue" << endl;
	cout << "7  - push to queue" << endl;
	cout << "8  - pop from queue" << endl;
	cout << "9  - write queue" << endl;

	cout << "10 - create deque" << endl;
	cout << "11 - push_back to deque" << endl;
	cout << "12 - push_front to deque" << endl;
	cout << "13 - pop_back from deque" << endl;
	cout << "14 - pop_front to deque" << endl;
	cout << "15 - write from right to left" << endl;
	cout << "16 - write from left to right" << endl;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	my_stack *stack = 0;
	my_queue *queue = 0;
	my_deque *deque = 0;
	//stack
	//my_stack *stack = 0;
	//for (int i = 0; i < 11; i++)
	//	push(stack, i);
	//write(stack);
	//queue
	//my_queue *queue = 0;
	//generate(queue, 10);
	//push(queue, 20);
	//write(queue);
	//pop(queue);
	//write(queue);
	//deque
	int num = 1;
	while (num) {
		cin >> num;
		switch (num) {
			case 1: {
				int n;
				cout << "Write num: ";
				cin >> n;
				push(stack, n);
				break;
			}
			case 2: pop(stack); break;
			case 3: cout << top(stack) << endl; break;
			case 4: reverse(stack); break;
			case 5: write(stack); break;
			case 6: {
				int n;
				cout << "Write num: ";
				cin >> n;
				generate(queue, n);
				break;
			}
			case 7: {
				int n;
				cout << "Write num: ";
				cin >> n;
				push(queue, n);
				break;
			}
			case 8: pop(queue); break;
			case 9: write(queue); break;
			case 10: {
				int n;
				cout << "Write num: ";
				cin >> n;
				create(deque, n);
				break;
			}
			case 11: {
				int n;
				cout << "Write num: ";
				cin >> n;
				push_back(deque, n);
				break;
			}
			case 12: {
				int n;
				cout << "Write num: ";
				cin >> n;
				push_front(deque, n);
				break;
			}
			case 13: pop_back(deque); break;
			case 14: pop_front(deque); break;
			case 15: write_rl(deque); break;
			case 16: write_lr(deque); break;
		}
	}
	system("pause");
	return 0;
}