#pragma once
#include <iostream>

using namespace std;

struct queue_node {
	int data;
	queue_node *next;
};
struct my_queue {
	queue_node *first;
	queue_node *last;
};
void generate(my_queue *&queue, int data);
void push(my_queue *&queue, int data);
void pop(my_queue *&queue);
void write(my_queue *queue);