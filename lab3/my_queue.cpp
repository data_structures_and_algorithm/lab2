#include "my_queue.h"

void generate(my_queue *&queue, int data)
{
	queue = new my_queue;
	queue->first = new queue_node;
	queue->first->data = data;
	queue->last = queue->first;
	queue->last->next = 0;
}
void push(my_queue *&queue, int data)
{
	if (!queue) {
		generate(queue, data);
		return;
	}
	queue_node *p = new queue_node;
	p->data = data;
	p->next = 0;
	queue->last->next = p;
	queue->last = p;
}
void pop(my_queue *&queue)
{
	if (!queue)
		return;
	if (!queue->first->next) {
		delete queue->first;
		delete queue;
		queue = 0;
		return;
	}

	queue_node *p = queue->first;
	queue->first = queue->first->next;
	delete p;
}
void write(my_queue *queue)
{
	if (!queue)
		return;
	queue_node *p = queue->first;
	while (p) {
		cout << p->data << " ";
		p = p->next;
	}
	cout << endl;
}