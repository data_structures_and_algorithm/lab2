#include "my_deque.h"

void create(my_deque *&deque, int data)
{
	deque = new my_deque;
	deque_node *p = new deque_node;
	p->data = data;
	p->next = 0;
	p->prev = 0;
	deque->first = p;
	deque->last = p;
}
void push_back(my_deque *&deque, int data)
{
	if (!deque) {
		create(deque, data);
		return;
	}
	deque_node *p = new deque_node;
	p->data = data;
	p->next = 0;
	p->prev = deque->last;
	deque->last->next = p;
	deque->last = p;
}
void push_front(my_deque *&deque, int data)
{
	if (!deque) {
		create(deque, data);
		return;
	}
	deque_node *p = new deque_node;
	p->data = data;
	p->next = deque->first;
	p->prev = 0;
	deque->first->prev = p;
	deque->first = p;
}
void pop_back(my_deque *&deque)
{
	if (!deque)
		return;
	if (!deque->last->prev) {
		delete deque->last;
		delete deque;
		deque = 0;
		return;
	}
	deque_node *p = deque->last;
	deque->last = deque->last->prev;
	deque->last->next = 0;
	delete p;
}
void pop_front(my_deque *&deque)
{
	if (!deque)
		return;
	if (!deque->first->next) {
		delete deque->first;
		delete deque;
		deque = 0;
		return;
	}
	deque_node *p = deque->first;
	deque->first = deque->first->next;
	deque->first->prev = 0;
	delete p;
}
void write_rl(my_deque *deque)
{
	if (!deque)
		return;
	deque_node *p = deque->first;
	while (p) {
		cout << p->data << " ";
		p = p->next;
	}
	cout << endl;
}
void write_lr(my_deque *deque)
{
	if (!deque)
		return;
	deque_node *p = deque->last;
	while (p) {
		cout << p->data << " ";
		p = p->prev;
	}
	cout << endl;
}